pub fn solve(input: String) {
//     let input = "seeds: 79 14 55 13

// seed-to-soil map:
// 50 98 2
// 52 50 48

// soil-to-fertilizer map:
// 0 15 37
// 37 52 2
// 39 0 15

// fertilizer-to-water map:
// 49 53 8
// 0 11 42
// 42 0 7
// 57 7 4

// water-to-light map:
// 88 18 7
// 18 25 70

// light-to-temperature map:
// 45 77 23
// 81 45 19
// 68 64 13

// temperature-to-humidity map:
// 0 69 1
// 1 0 69

// humidity-to-location map:
// 60 56 37
// 56 93 4";


    let mut maps = input.split("map:\n");
    let mut seeds: Vec<u64> = maps.next().unwrap().split("\n").next().unwrap().split(" ") //list of seeds
    .map(|seed| seed.parse() ).flatten().collect();

    let mut curr_map = maps.next();
    while curr_map.is_some() {
        let mut new_seeds = seeds.clone();
        let _ = curr_map.unwrap().lines()
                    .map(|line| {
                        println!("{}", line);
                        if line.len() > 1 && line.chars().next().unwrap().is_ascii_digit(){
                            let mut items = line.split(" ").map(|x| x.parse::<u64>().unwrap());
                            let destination = items.next().unwrap();
                            let source = items.next().unwrap();
                            let range = items.next().unwrap();

                            for (i,seed) in seeds.iter().enumerate() {
                                //println!("{} {} {} {}", seed, source, destination, range);
                                if (source..source + range).contains(&seed) {
                                    new_seeds[i] -= source;
                                    new_seeds[i] = destination + new_seeds[i];
                                }
                            };
                        }
                    }).count();
        seeds = new_seeds.clone();
        println!("new map, processed seeds: {:?}", seeds);
        curr_map = maps.next();
    }

    println!("Result part 1: {}", &seeds.iter().min().unwrap());
}