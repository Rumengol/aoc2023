pub fn solve(input:String){
//     let input = "0 3 6 9 12 15
// 1 3 6 10 15 21
// 10 13 16 21 30 45";

    let predictions = input.lines().map(|line|{
        let line : Vec<i32> = line.split(" ").map(|n| n.parse().unwrap()).collect();
        let solved = solve_nums(&line);
        (line.iter().next().unwrap() - solved.0, line.iter().last().unwrap() + solved.1)
    });

    println!("Result part 1: {}", predictions.clone().map(|x| x.1).sum::<i32>());
    println!("Result part 2: {}", predictions.map(|x| x.0).sum::<i32>());
}

fn solve_nums(numbers: &Vec<i32>) -> (i32,i32) {
    let mut next_nums: Vec<i32> = Vec::new();
    for i in 0..numbers.len() -1{
        next_nums.push(numbers[i+1] - numbers[i]);
    }

    if next_nums.iter().all(|x| x == &0){
        (0,0)
    } else {
        let solved_next = solve_nums(&next_nums);
        (*next_nums.iter().next().unwrap() - solved_next.0,
        *next_nums.iter().last().unwrap() + solved_next.1)
    }
}