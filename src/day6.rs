use itertools::intersperse;

pub fn solve(input:String){
//     let input = "Time:      7  15   30
// Distance:  9  40  200";

    let mut lines = input.lines();
    let mut times = lines.next().unwrap().split_whitespace().skip(1);
    let mut distances = lines.next().unwrap().split_whitespace().skip(1);

    let long_time: u64 = times.clone().collect::<String>().parse().unwrap();
    let long_distance: u64 = distances.clone().collect::<String>().parse().unwrap();

    let mut possible_solutions = vec![0;4];
    let mut possible_solutions_counter = 0;
    let mut i = 0;
    //println!("{} {}", times.next().unwrap(), distances.next().unwrap());
    loop{
        let time_opt = times.next();
        let curr_time: u32;
        if time_opt.is_none(){ break;}
        else { curr_time = time_opt.unwrap().parse().unwrap(); }
        let curr_best = distances.next().unwrap().parse().unwrap();

        for time in 1..curr_time{
            if time * (curr_time - time) > curr_best{
                possible_solutions[i] += 1;
            }
        }

        for time in 1..long_time{
            if time * (long_time - time) > long_distance{
                possible_solutions_counter += 1
            }
            else if possible_solutions_counter >5 { break; }
        }
        i+=1;
    }

    println!("Result part 1: {}", possible_solutions.iter().product::<u32>());
    println!("Result part 2: {}", possible_solutions_counter);
}