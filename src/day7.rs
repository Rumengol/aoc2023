use std::collections::HashMap;


pub fn solve(input:String){
//     let input = "2345A 1
// Q2KJJ 13
// Q2Q2Q 19
// T3T3J 17
// T3Q33 11
// 2345J 3
// J345A 2
// 32T3K 5
// T55J5 29
// KK677 7
// KTJJT 34
// QQQJA 31
// JJJJJ 37
// JAAAA 43
// AAAAJ 59
// AAAAA 61
// 2AAAA 23
// 2JJJJ 53
// JJJJ2 41";
    let mut card_map: HashMap<char, u8> = HashMap::new();

    let score_bids: HashMap<u64, u32> = input.lines()
                    .map(|line|{
                        card_map.clear();

                        let mut tmp = line.split(" ");
                        let cards = tmp.next().unwrap();

                        let _ = cards.chars()
                        .map(|c| {
                            card_map.insert(c, card_map.get(&c).unwrap_or(&0) + 1);
                        }).count();
                        let mut tmp2 = card_map.values().collect::<Vec<&u8>>();
                        let jokers: u64 = *card_map.get(&'J').unwrap_or(&0) as u64;
                        tmp2.sort();
                                                
                        let mut score: u64 = 10u64.pow(10) * match tmp2[..]{
                            [&5] => 8,
                            [&1,&4] => 7 + if jokers > 0 {1} else {0},
                            [&2,&3] => 6 + if jokers > 0 {2} else {0},
                            [&1,&1,&3] => 5 + if jokers > 0 {2} else {0},
                            [&1,&2,&2] => 4 + if jokers > 0 {jokers + 1} else {0},
                            [&1,&1,&1,&2] => 3 + if jokers > 0 {2} else {0},
                            _ => 2 + jokers
                        };

                        score += cards.chars().enumerate()
                            .map(|(i, c)|{

                            (10u64.pow( 8-(i*2) as u32)) * match c {
                                c if c.is_ascii_digit() => c.to_digit(10).unwrap(),
                                'T' => 10,
                                'J' => 1,
                                'Q' => 12,
                                'K' => 13,
                                'A' => 14,
                                _ => panic!("edge case spotted: {}", c)
                            } as u64
                        }).sum::<u64>();


                        
                        println!("To Score: {}", score);


                        let bid = tmp.next().unwrap().parse().unwrap();
                        (score, bid)
                    }).collect();
                    
    let mut scores = score_bids.keys().collect::<Vec<&u64>>();
    scores.sort();

    println!("Results part 2 : {}", scores.iter().enumerate()
                    .map(|(i, key)|
                        score_bids[key] * (i + 1) as u32).sum::<u32>());

}
