use std::env;
use std::fs;

pub mod day1;
pub mod day2;
pub mod day3;
pub mod day4;
pub mod day5;
pub mod day6;
pub mod day7;
pub mod day8;
pub mod day9;
pub mod day10;
fn main() {
    //env::set_var("RUST_BACKTRACE", "1");
    let days: Vec<fn(String)> = vec![day1::solve, day2::solve, day3::solve, day4::solve, day5::solve, day6::solve, day7::solve, day8::solve, day9::solve, day10::solve];
    let args: Vec<String> = env::args().collect();

    let day:u32 = args[1].parse().expect("Not a number");
    
    let input = fs::read_to_string(format!("./input/day{day}.txt")).unwrap_or(String::from("File not readable."));
    days[(day - 1) as usize](input);
}