use std::collections::HashMap;
use num::integer::lcm;

pub fn solve(input: String){
//     let input = "LR

// 11A = (11B, XXX)
// 11B = (XXX, 11Z)
// 11Z = (11B, XXX)
// 22A = (22B, XXX)
// 22B = (22C, 22C)
// 22C = (22Z, 22Z)
// 22Z = (22B, 22B)
// XXX = (XXX, XXX)";

    let mut lines = input.lines();

    let directions: Vec<char> = lines.next().unwrap().chars().collect();

    let instructions: HashMap<&str, (&str, &str)>= lines.skip(1).map(|line|{
        let mut tmp = line.split(" = ");
        let key = tmp.next().unwrap();
        let value = tmp.next().unwrap();

        (key, (value.get(1..4).unwrap(), value.get(6..9).unwrap()))
    }).collect();

    let curr_locations: Vec<&&str> = instructions.keys().filter(|&k| k.ends_with('A')).collect();

    let mut min_steps: Vec<u32> = Vec::new();

    for location in curr_locations {
        let mut i = 0;
        let mut steps = 0;
        let mut loc = location.clone();
        while !loc.ends_with('Z'){
            loc = match directions[i]{
                    'R' => instructions[loc].1,
                    'L' => instructions[loc].0,
                    _ => "" 
                };
            i += 1;
            steps += 1;
            
            if i == directions.len(){i = 0;}
        }
        min_steps.push(steps)
    }

    let mut lowest_step: u64 = 1;
    for i in min_steps{
        lowest_step = lcm(lowest_step, i as u64);
    }

    println!("Reached destinations in at least {} steps", lowest_step);
}