use std::cmp::{max, min};
#[allow(dead_code)]
struct Number {
    value: u32,
    line: usize,
    start: usize,
    end: usize,
}

pub fn solve(input:String){

    let lines: Vec<&str> = input.split("\n").collect();
    let mut all_numbers: Vec<Number> = Vec::new();
    let mut result: Vec<u32> = Vec::new();
    let mut gear_ratios: Vec<u32> = Vec::new();

    for (i,line) in lines.iter().enumerate(){
        for (j,c) in line.chars().enumerate(){
            if c != '.' && !c.is_digit(10){
                all_numbers.append(&mut get_numbers_around(&c, &lines, (i,j), &all_numbers, &mut gear_ratios));
            }
        }
    }
    for num in all_numbers{
        result.push(num.value);
    }
    println!("Result part 1: {}", result.iter().sum::<u32>()); 
    println!("Result part 2: {}", gear_ratios.iter().sum::<u32>())
}

fn get_numbers_around(target: &char, lines: &Vec<&str>, coordinate: (usize,usize), all_numbers: &Vec<Number>, gear_ratios: &mut Vec<u32>) -> Vec<Number> {
    let mut numbers: Vec<Number> = Vec::new();
    for i in -1..=1 as isize{
        for j in -1..=1 as isize{
            let mut new_coord = (
                if coordinate.0 as isize + i < 0 {0}
                else if coordinate.0 as isize + i == lines[0].len() as isize {coordinate.0}
                else { (coordinate.0 as isize + i) as usize},
                if coordinate.1 as isize + j < 0 {0}
                else if coordinate.1 as isize + j == lines[0].len() as isize {coordinate.1}
                else {(coordinate.1 as isize + j) as usize}
            );
            if lines[new_coord.0].chars().nth(new_coord.1).unwrap().is_digit(10){
                while new_coord.1 > 0 && lines[new_coord.0].chars().nth(new_coord.1).unwrap().is_digit(10){
                    new_coord.1 = (new_coord.1 as isize - 1) as usize;
                }
                if !(new_coord.1 == 0 && lines[new_coord.0].chars().nth(new_coord.1).unwrap().is_digit(10)){
                    new_coord.1+= 1;
                }
                if !numbers.iter().any(|nb| nb.line == new_coord.0 && nb.start == new_coord.1) && !all_numbers.iter().any(|nb| nb.line == new_coord.0 && nb.start == new_coord.1){
                    numbers.push(parse_number(lines, new_coord));
                }
            }
        }
    }
    if numbers.len() == 2 && target == &'*'{
        gear_ratios.push(numbers[0].value * numbers[1].value)
    }
    numbers
}

fn parse_number(lines: &Vec<&str>, coordinate: (usize,usize)) -> Number {
    let mut chars_of_interest = lines[coordinate.0][coordinate.1..].chars();
    let mut i = coordinate.1;
    loop{
        let curr_char: char = chars_of_interest.next().unwrap_or('.');
        if !curr_char.is_digit(10){
            return Number{
                value: lines[coordinate.0][coordinate.1..i].parse().unwrap(),
                line: coordinate.0,
                start: coordinate.1,
                end: i
            }; 
        }
        i+=1;
    }
}

//First approach, proved to not be suited to part 2
#[allow(dead_code)]
fn symbol_around(target: &Number, above_line: Option<&str>, below_line: Option<&str>) -> bool {
    if let Some(abline) = above_line{
        if let Some(_) =abline[max(target.start-1,0) as usize..min(target.end +1, abline.len() -1)].chars().enumerate()
            .find(|&c| c.1 != '.' && !c.1.is_digit(10)){
                return true
            }
    }
    if let Some(bline) = below_line{
        if let Some(_) =bline[max(target.start-1 ,0) as usize..min(target.end +1, bline.len() -1)].chars().enumerate()
            .find(|&c| c.1 != '.' && !c.1.is_digit(10)){
                return true
            }
    }
    false
} 
