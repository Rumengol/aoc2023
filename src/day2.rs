use std::collections::HashMap;

pub fn solve(input: String){
    let lines: std::str::Split<'_, &str> = input.split("\n");
    let mut possible_games: Vec<u32> = Vec::new();
    let mut minimum_sets: Vec<u32> = Vec::new();

    let thresholds: HashMap<&str, u32> = HashMap::from([("red",12),("green",13),("blue",14)]);

    for line in lines{
        let mut tmp: std::str::Split<'_, &str> = line[5..].split(": ");
        let game_id: u32 = tmp.next().unwrap().parse().unwrap();
        possible_games.push(game_id);
        let rounds = tmp.next().unwrap().split("; ");
        
        let mut maximums: HashMap<&str, u32> = HashMap::from([("red",0),("green",0),("blue",0)]);

        for round in rounds{
            let mut possible = true;
            let draws = round.split(", ");

            for draw in draws{
                let mut tmp = draw.split(" ");
                let count: u32 = tmp.next().unwrap().parse().unwrap();
                let color: &str = tmp.next().unwrap();

                if maximums.get(color).unwrap() < &count{
                    maximums.insert(color, count);
                }

                if &count > thresholds.get(color).unwrap(){
                    if possible {
                        possible_games.pop();
                        possible = false;
                    }
                }
            }
        }
        minimum_sets.push(maximums.values().product());
    }
    let sum_pos: u32 = possible_games.iter().sum();
    let sum_min: u32 = minimum_sets.iter().sum();
    println!("{:?}", minimum_sets);
    println!("Result part 1: {}", sum_pos);
    println!("Result part 2: {}", sum_min)
}