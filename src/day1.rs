pub fn solve(input: String){
    let lines = input.split("\n");
    let nums = vec!["zero","one","two","three","four","five","six","seven","eight","nine"];
    let mut calibrating_values1: Vec<u32> = Vec::new();
    let mut calibrating_values2: Vec<u32> = Vec::new();

    for line in lines{
        let mut tempval1: Vec<u32> = Vec::new();
        let mut tempval2: Vec<u32> = Vec::new();
        for (i,letter) in line.chars().enumerate(){
            if letter.is_digit(10){
                tempval1.push(letter.to_digit(10).unwrap());
                tempval2.push(letter.to_digit(10).unwrap());
            } else {
                for (j,num) in nums.iter().enumerate() {
                    let end = if i+5 < line.len() {i+5} else {line.len()};
                    if line[i..end].starts_with(num){
                        tempval2.push(j as u32);
                    }
                }
            }
        }
        calibrating_values1.push(*tempval1.first().unwrap() * 10 + tempval1.last().unwrap());
        calibrating_values2.push(*tempval2.first().unwrap() * 10 + tempval2.last().unwrap());
    }
    let sum1: u32 = calibrating_values1.iter().sum();
    let sum2: u32 = calibrating_values2.iter().sum();
    println!("Result part 1: {}", sum1);
    println!("Result part 2: {}", sum2)


}
