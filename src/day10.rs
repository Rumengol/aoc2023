use std::collections::HashMap;

enum Direction {
    North,
    South,
    East,
    West
}

pub fn solve(input: String){
    let input = "..F7.
.FJ|.
SJ.L7
|F--J
LJ...";

    let mut coord_range: HashMap<(usize,usize), u32> = HashMap::new();

    let mut i = -1;
    let mut j = -1;

    let mut starting_coord: (usize, usize) = (0,0);

    let network: Vec<Vec<char>>= input.lines().map(|x| {
        i += 1;
        x.chars().map(|c| {
            j += 1;
            if c == 'S' {starting_coord = (i as usize,j as usize);}
            c
        }).collect()
    }).collect();

    if ['|','7','F'].contains(&network[starting_coord.0][starting_coord.1-1]){
        coord_range.insert((starting_coord.0, starting_coord.1-1), 1);
    }
    if ['|','J','L'].contains(&network[starting_coord.0][starting_coord.1+1]){
        coord_range.insert((starting_coord.0, starting_coord.1+1), 1);
    }
    if ['-','L','F'].contains(&network[starting_coord.0-1][starting_coord.1]){
        coord_range.insert((starting_coord.0-1, starting_coord.1), 1);
    }
    if ['-','7','J'].contains(&network[starting_coord.0+1][starting_coord.1]){
        coord_range.insert((starting_coord.0+1, starting_coord.1), 1);
    }

    

}

fn next_direction(pipe: char, coming_from:Direction) -> (isize,isize){
    match pipe{
        '.' => panic!("Not a pipe"),
        'S' => panic!("Should not get to the starting position"),
        '|' => if let Direction::North = coming_from {(0,-1)} else {(0,1)},
        '-' => if let Direction::East = coming_from {(-1,0)} else {(1,0)},
        'L' => if let Direction::North = coming_from {(1,0)} else {(0,1)},
        'J' => if let Direction::North = coming_from {(-1,0)} else {(0,1)},
        '7' => if let Direction::South = coming_from {(-1,0)} else {(0,-1)},
        'F' => if let Direction::South = coming_from {(1,0)} else {(0,-1)},
        _ => panic!("Not a pipe")
    }
}