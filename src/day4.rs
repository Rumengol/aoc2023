pub fn solve(input: String){
//     let input ="Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
// Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
// Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
// Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
// Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
// Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11";

    let lines = input.split("\n");
    let mut score = 0;
    let mut copies = vec![1; lines.clone().count()];

    for (i,line) in lines.enumerate(){
        let mut wins = 0;
        let mut numbers = line.split(": ").skip(1).next().unwrap().split(" | ");
        let winning_numbers = numbers.next().unwrap().split(" ");
        let scratched_numbers = numbers.next().unwrap().split(" ");
        
        for number in winning_numbers{
            if number.len() >= 1 &&scratched_numbers.clone().any(|num| num == number){
                wins += 1
            }
        }
        for j in 1..=wins{
            copies[i + j] += 1 * &copies[i];
        }
        score += if wins > 0 {1 + 2**&wins} else {0};
    }
    println!("Total score:{}", score);
    println!("Copies: {}", copies.iter().sum::<u32>());
}